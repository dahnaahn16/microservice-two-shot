# Wardrobify

Team:

* Dahna - Shoes microservice
* Paarth - Hats microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

First I wrote out the models and used an encoder to write out all the restful APIs in the views.py for just the shoes component. In order to integrate the wardrobe microservice I added a binVO and updated the poller so the data could successfully transfer over. The shoes model had a foreign key linked to the binVO model that successfully transfered over the data from the bins. In the views i used the get extra data function to integrate the bins data to the shoe data.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

I created a model for the specific location of hats within a wardrobe, and a model for the actual hats with the attributes of fabric, style, color, picture and location within the wardrobe. The location model data was corellated to the wardrobe microservice with the help of the get_extra_data function in the hat encoder. Data was recorded via polling.
