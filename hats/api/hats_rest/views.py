from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Hats, LocationVO
# Create your views here.

class LocationEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        ]

class HatEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "id",
        "location",
    ]
    def get_extra_data(self,o):
        return{"location": o.location.closet_name}

@require_http_methods(["GET", "POST"])
def show_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()
            return JsonResponse(
                {"hats": hats},
                encoder=HatEncoder,
                safe=False,
            )
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "invalid location id"},
                status=400
            )
        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def hats_list(request, id):
    if request.method == "GET":
        try:
            hats = Hats.objects.get(id=id)
            return JsonResponse(
               hats,
               encoder=HatEncoder,
               safe=False
            )
        except Hats.DoesNotExist:
            response = JsonResponse({"message":"Does not exist"})
            response.status_code = 404
            return response
    elif request.method=="DELETE":
        try:
            hats = Hats.objects.get(id=id)
            hats.delete()
            return JsonResponse(
                Hats,
                encoder=HatEncoder,
                safe=False
            )
        except Hats.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
