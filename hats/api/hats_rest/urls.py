from django.urls import path
from .views import hats_list, show_hats

urlpatterns = [
    path('hats/', show_hats, name='show_hats'),
    path("hats/<int:id>/", hats_list, name="hats_list"),
]
