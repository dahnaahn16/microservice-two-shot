from django.db import models
from django.urls import reverse

class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.closet_name


class Shoes(models.Model):
    shoe_brand = models.CharField(max_length=100)
    shoe_name = models.CharField(max_length=100)
    shoe_color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )


    class Meta:
        ordering = ("shoe_brand",)

    def __str__(self):
        return self.shoe_name

    def get_api_url(self):
        return reverse("api_show_shoes", kwargs={"pk": self.pk})
