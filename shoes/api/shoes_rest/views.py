from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoes, BinVO


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "closet_name"]

class ShoeEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "shoe_brand",
        "shoe_name",
        "shoe_color",
        "picture_url",
        "id",
        "bin",
    ]
    def get_extra_data(self,o):
        return{"bin": o.bin.closet_name}

@require_http_methods(["GET", "POST"])
def api_show_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoes.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoes.objects.all()
            return JsonResponse(
                {"shoes": shoes},
                encoder=ShoeEncoder,
                safe=False,
            )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "invalid bin id"},
                status=400
            )
        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def api_list_shoes(request, id):
    if request.method=="GET":
        try:
            shoes = Shoes.objects.get(id=id)
            return JsonResponse(
                shoes,
                encoder=ShoeEncoder,
                safe=False
            )
        except Shoes.DoesNotExist:
            response = JsonResponse({"message":"Does not exist"})
            response.status_code = 404
            return response
    elif request.method=="DELETE":
        try:
            shoes = Shoes.objects.get(id=id)
            shoes.delete()
            return JsonResponse(
                shoes,
                encoder=ShoeEncoder,
                safe=False
            )
        except Shoes.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
