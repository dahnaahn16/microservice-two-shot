import './Shoes.css'

import React, { useEffect, useState } from 'react'

function ShoeForm(){
    const[bins, setBins] = useState([])

    const[shoeBrand, setShoeBrand] = useState('')
    const[shoeName, setShoeName] = useState('')
    const[shoeColor, setShoeColor] = useState('')
    const[pictureURL, setPictureURL] = useState('')
    const[binName, setBinName] = useState('')

    const handleShoeBrandChange = (event) => {
        const value = event.target.value;
        setShoeBrand(value)
    }

    const handleShoeNameChange = (event) => {
        const value = event.target.value;
        setShoeName(value)
    }

    const handleShoeColorChange = (event) => {
        const value = event.target.value;
        setShoeColor(value)
    }

    const handlePictureURLChange = (event) => {
        const value = event.target.value;
        setPictureURL(value)
    }

    const handlebinNameChange = (event) => {
        const value = event.target.value;
        setBinName(value)
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data={
        shoe_brand: shoeBrand,
        shoe_name: shoeName,
        shoe_color: shoeColor,
        picture_url: pictureURL,
        bin: binName
        }
        const shoesURL = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        };
        const response = await fetch(shoesURL, fetchConfig);
        if (response.ok){
            const newShoe = await response.json();
            console.log('newShoe', newShoe)
            setShoeBrand('')
            setShoeName('')
            setShoeColor('')
            setPictureURL('')
            setBinName('')
        }
    }
    
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/'
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setBins(data.bins)
    }
}
useEffect(() => {
    fetchData();
}, []);

function refreshPage(){
  window.location.reload(true)
}

    return(
        <div className="row">
      <div className="offset-3">
        <div className="shadow p-4 mt-4">
          <h1>create a new shoe</h1>
          <form onSubmit={handleSubmit} id="create-location-form">

            <div className="form-floating mb-3">
              <input value={shoeBrand} onChange={handleShoeBrandChange} placeholder="shoebrand" required type="text" name="shoebrand" id="shoebrand" className="form-control"/>
              <label htmlFor="brand">shoe brand</label>
            </div>


            <div className="form-floating mb-3">
              <input value={shoeName} onChange={handleShoeNameChange} placeholder="shoename" required type="text" name="shoename" id="shoename" className="form-control"/>
              <label htmlFor="shoename">shoe name</label>
            </div>


            <div className="form-floating mb-3">
              <input value={shoeColor} onChange={handleShoeColorChange} placeholder="shoecolor" required type="text" name="shoecolor" id="shoecolor" className="form-control"/>
              <label htmlFor="shoecolor">shoe color</label>
            </div>

            <div className="form-floating mb-3">
              <input value={pictureURL} onChange={handlePictureURLChange} placeholder="pictureurl" required type="url" name="pictureurl" id="pictureurl" className="form-control"/>
              <label htmlFor="pictureurl">picture url</label>
            </div>


            <div className="mb-3">
              <select onChange={handlebinNameChange} required name="bin" id="bin" className="form-select">
                <option value={binName}>Choose a bin</option>
                {bins.map(bin =>{
                    return(
                        <option key={bin.href} value={bin.href}>
                            {bin.closet_name}
                        </option>
                    )
                })}
              </select>
            </div>

            <button onClick={refreshPage} className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    )
}


export default ShoeForm
