import './Hats.css'

import React, { useEffect, useState } from 'react'

function HatsForm(){
    const[Location, setLocation] = useState([])

    const[fabric, setFabric] = useState('')
    const[style, setStyle] = useState('')
    const[color, setColor] = useState('')
    const[pictureURL, setPictureURL] = useState('')
    const[locationName, setLocationName]=useState('')


    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value)
    }

    const handleStyleChange = (event) => {
        const value = event.target.value;
        setStyle(value)
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value)
    }

    const handlePictureURLChange = (event) => {
        const value = event.target.value;
        setPictureURL(value)
    }

    const handleLocationName = (event) => {
      const value = event.target.value;
      setLocationName(value)
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data={
        fabric: fabric,
        style_name: style,
        color: color,
        picture_url: pictureURL,
        location: locationName,
        }

        const hatsURL = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        };
        const response = await fetch(hatsURL, fetchConfig);
        if (response.ok){
            const newHat = await response.json();
            console.log('newHat', newHat)
            setStyle('')
            setFabric('')
            setColor('')
            setLocation('')
            setPictureURL('')
            setLocationName('')
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/'
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setLocation(data.locations)
    }
}
useEffect(() => {
    fetchData();
}, []);

function refreshPage(){
  window.location.reload(true)
}

    return(
        <div className="row">
      <div className="offset-3">
        <div className="shadow p-4 mt-4">
          <h1>create a new hat</h1>
          <form onSubmit={handleSubmit} id="create-location-form">

            <div className="form-floating mb-3">
              <input value={fabric} onChange={handleFabricChange} placeholder="fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
              <label htmlFor="fabric">Fabric</label>
            </div>


            <div className="form-floating mb-3">
              <input value={style} onChange={handleStyleChange} placeholder="style" required type="text" name="style" id="style" className="form-control"/>
              <label htmlFor="style">style</label>
            </div>


            <div className="form-floating mb-3">
              <input value={color} onChange={handleColorChange} placeholder="color" required type="text" name="color" id="color" className="form-control"/>
              <label htmlFor="color">color</label>
            </div>



            <div className="form-floating mb-3">
              <input value={pictureURL} onChange={handlePictureURLChange} placeholder="pictureurl" required type="url" name="pictureurl" id="pictureurl" className="form-control"/>
              <label htmlFor="pictureurl">picture url</label>
            </div>

            <div className="mb-3">
              <select onChange={handleLocationName} required name="location" id="location" className="form-select">
                <option value={locationName}>Choose a location</option>
                {Location.map(location =>{
                    return(
                        <option key={location.href} value={location.href}>
                            {location.closet_name}
                        </option>
                    )
                })}
              </select>
            </div>

            <button onClick={refreshPage} className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    )
}


export default HatsForm
