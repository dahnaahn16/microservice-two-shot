import './Shoes.css'
import { NavLink, Outlet } from 'react-router-dom';

function ShoeIndex() {
    return (
    <div>
      <main style={{ padding: "1rem" }}>
        <h1> ← Select a shoe </h1>
      </main>

      <NavLink to='./ShoeForm'>
          <button className="create-shoe-button"> Create a shoe</button>
      </NavLink>

      <Outlet />
    </div>
    );
  }

  export default ShoeIndex;
