import './Hats.css'
import { NavLink, Outlet } from 'react-router-dom';

function HatIndex() {
    return (
    <div>
      <main style={{ padding: "1rem" }}>
        <h1> ← Select a hat </h1>
      </main>

      <NavLink to='./HatsForm'>
          <button className="create-hat-button"> Create an entry</button>
      </NavLink>

      <Outlet />
    </div>
    );
  }

  export default HatIndex;
