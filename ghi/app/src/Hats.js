import React, { useState, useEffect } from 'react'
import { NavLink, Outlet } from 'react-router-dom';
import './Hats.css'
import './HatsForm'

function Hats (props) {
  const [hats, setHats] = useState([]);
  const fetchData = async () => {
    const url = 'http://localhost:8090/api/hats/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setHats(data.hats)
      console.log(data)
      }
    }


  useEffect(() => {
    fetchData();
  }, []);

  const handleDelete = async(id) => {
    const confirm = window.confirm("Are you sure you want to delete this item?")
    if(confirm){
        const url=`http://localhost:8090/api/hats/${id}`;
        const response = await fetch(url,{
            method: 'DELETE',
        })
        if(response.ok){
            document.location.href = 'http://localhost:3000/hats'
    }
    }
}

if (hats === undefined){
    return null
}
  return(

    <div style={{ display: 'flex' }}>
    <nav style={{
      borderRight: 'solid 1px',
      padding: '2rem',
      paddingRight: '5rem'
    }}>
        {hats.map((hat) => (
        <NavLink
        style={( {isActive} ) => {
          return {
            display: "block",

            color: isActive ? "blue" : "black",
            textDecoration: "none"
          };
        }}
        to={`/hats/${hat.id}`} key={hat.id}>
        <ul className="hats-list">
        <li style={{ display: 'flex', justifyContent: 'space-between' }}>
        <span>{hat.fabric}</span>
        <button className="delete-button" onClick={() => handleDelete(hat.id)}>🗑️</button>
        </li>
        </ul>
            </NavLink>
        ))}
    </nav>

    <Outlet />
    </div>

);

  }
export default Hats
