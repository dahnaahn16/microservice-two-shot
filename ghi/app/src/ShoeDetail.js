import { useParams } from 'react-router-dom';
import React, { useState, useEffect } from 'react'


function ShoeDetail(){
    const {id} = useParams();

    const [shoes, setShoes] = useState({});

    const fetchData = async () => {
        const url = `http://localhost:8080/api/shoes/${id}`
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setShoes(data)
    }
}


useEffect(() => {
    fetchData();
}, [id]);


    return(
        <div style={{
            marginTop: "4rem",
            marginLeft: "2rem"
        }} className="container">
        <div className="flip-card">
            <div className="flip-card-inner">
                <div className="flip-card-front">

                <img style={{width:"300px", marginLeft:"4rem"}}
                src={shoes.picture_url}
                alt={shoes.shoe_name}
                />
                <p style={{color:"grey"}}>hover me</p>
                </div>
                <div className="flip-card-back">
                    <p className="title">DETAILS</p>
                    <p> Shoe name: {shoes.shoe_name} </p>
                    <p> Shoe brand: {shoes.shoe_brand} </p>
                    <p> Shoe color: {shoes.shoe_color} </p>
                    <p>Bin: {shoes.bin}</p>
                </div>
            </div>
        </div>

            {/* <div style={{textAlign: "center"}}>
                <p> Shoe name: {shoes.shoe_name} </p>
                <p> Shoe brand: {shoes.shoe_brand} </p>
                <p> Shoe color: {shoes.shoe_color} </p>
                <img style={{width:"300px"}}
                src={shoes.picture_url}
                alt={shoes.shoe_name}
                />
                <p>Bin: {shoes.bin}</p>
            </div> */}

        </div>
    )

}


export default ShoeDetail
