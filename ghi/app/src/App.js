import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeIndex from './ShoeIndex'
import Shoes from './Shoes'
import ShoeDetail from './ShoeDetail'
import ShoeForm from './ShoeForm';
import HatsIndex from './HatsIndex'
import Hats from './Hats'
import HatsDetail from './HatsDetail'
import HatsForm from './HatsForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes/" element={<Shoes />} >
              <Route path="ShoeForm/" element={<ShoeForm />}/>
              <Route index element={<ShoeIndex />} />
              <Route path=':id' element={<ShoeDetail />} />
          </Route>
          <Route path="hats/" element={<Hats />} >
              <Route path="HatsForm/" element={<HatsForm />}/>
              <Route index element={<HatsIndex />} />
              <Route path=':id' element={<HatsDetail />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
