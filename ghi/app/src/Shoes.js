import React, { useState, useEffect } from 'react'
import { NavLink, Outlet } from 'react-router-dom';
import './Shoes.css'
import './ShoeForm'

function Shoes(props){
    const [shoes, setShoes] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes/'
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setShoes(data.shoes)

    }

}
useEffect(() => {
    fetchData();
}, []);

const handleDelete = async(id) => {
    const confirm = window.confirm("Are you sure you want to delete this shoe?")
    if(confirm){
        const url=`http://localhost:8080/api/shoes/${id}`;
        const response = await fetch(url,{
            method: 'DELETE',
        })
        if(response.ok){
            document.location.href = 'http://localhost:3000/shoes'
    }
    }
}

if (shoes === undefined){
    return null
}
    return (
        <>


        <div style={{ display: 'flex' }}>
        <nav style={{
          borderRight: 'solid 1px',
          padding: '2rem',
          paddingRight: '5rem'
        }}>
            {shoes.map((shoe) => (
            <NavLink
            style={( {isActive} ) => {
              return {
                display: "block",

                color: isActive ? "blue" : "black",
                textDecoration: "none"
              };
            }}
            to={`/shoes/${shoe.id}`} key={shoe.id}>
            <ul className="shoe-list">
            <li style={{ display: 'flex', justifyContent: 'space-between' }}>
            <span>{shoe.shoe_name}</span>
            <button className="delete-button" onClick={() => handleDelete(shoe.id)}>🗑️</button>
            </li>
            </ul>
                </NavLink>
            ))}
        </nav>

        <Outlet />
        </div>

        </>
    );
}

export default Shoes
