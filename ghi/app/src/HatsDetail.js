import { useParams } from 'react-router-dom';
import React, { useState, useEffect } from 'react'


function HatsDetail(){
    const {id} = useParams();

    const [hats, setHats] = useState({});

    const fetchData = async () => {
        const url = `http://localhost:8090/api/hats/${id}`
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setHats(data)
        console.log('data::::', data)
    }
}


useEffect(() => {
    fetchData();
}, [id]);


    return(
        <div style={{
            marginTop: "4rem",
            marginLeft: "2rem"
        }} className="container">
        <div className="flip-card">
            <div className="flip-card-inner">
                <div className="flip-card-front">

                <img style={{width:"300px", marginLeft:"4rem"}}
                src={hats.picture_url}
                alt={hats.style}
                />
                <p style={{color:"grey"}}>hover me</p>
                </div>
                <div className="flip-card-back">
                    <p className="title">DETAILS</p>
                    <p> Fabric: {hats.fabric} </p>
                    <p> Color: {hats.color} </p>
                    <p> Location in Wardrobe: {hats.location} </p>
                </div>
            </div>
        </div>
        </div>
    )

}


export default HatsDetail
